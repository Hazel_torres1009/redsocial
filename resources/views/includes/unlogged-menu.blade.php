
    <ul class="right hide-on-med-and-down">
    	<li>
            <a @if($routename=="NotFound"||$routename=="accound") href="{{ url(route('login')) }}" @else href="#" class="show-login-form" @endif>Login</a>
        </li>
        <li>
            <a @if($routename=="NotFound"||$routename=="accound") href="{{ url(route('auth')) }}" @else href="#" class="show-signup-form" @endif>Signup</a>
        </li>
    </ul>
    <ul id="nav-mobile" class="side-nav">
        <li>
            <a @if($routename=="NotFound"||$routename=="accound") href="{{ url(route('login')) }}" @else href="#" class="show-login-form" @endif>Login</a>
        </li>
        <li>
            <a @if($routename=="NotFound"||$routename=="accound") href="{{ url(route('auth')) }}" @else href="#" class="show-signup-form" @endif>Signup</a>
        </li>
        <li>
            <div class="collection-item no-padding">
                <script type="text/javascript" src=></script>
            </div>
        </li>
    </ul>