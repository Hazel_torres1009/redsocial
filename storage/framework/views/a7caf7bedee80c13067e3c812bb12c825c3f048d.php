<?php $__env->startSection('content'); ?>
    <div class="container row">
        <div class="col s12 no-padding mmargin-t">
            <?php if(isset($user) && $user != null): ?>
                <div class="col s12 white border-thin">
                    <div class="col s12 no-padding">
                        <div class="center">
                            <div class="col s12">
                                <a href="<?php echo e(url(route('show-photo'))); ?>" class="show-photo" data-photo-id="<?php echo e(encrypt($user->profile->avatar->id)); ?>">
                                    <img src="<?php echo e(asset(File::exists($user->profile->avatar->medium) ? $user->profile->avatar->medium : 'img/default_avatar.png')); ?>" width="200" class="circle z-depth-1 mmargin-tb profile-avatar-large">
                                </a>
                            </div>
                            <div class="col s12 no-padding">
                                <div class="col s12 m4 l4 margin-t">
                                    <button class="waves-effect pink waves-light btn d-block tooltipped" data-position="top" data-tooltip="Heart <i class='icon-heart pink-text lighteen-1'></i>" id="edit-profile-avatar">
                                        <span class="hide-on-small-only">Heart</span> <i class="icon-heart"></i>
                                    </button>
                                </div>
                                <div class="col s12 m4 l4 margin-t">
                                        <button class="waves-effect yellow waves-light btn d-block tooltipped" data-position="top" data-tooltip="Like <i class='icon-star yellow-text lighteen-1'></i>">
                                            <span class="hide-on-small-only">Like</span> <i class="icon-star"></i>
                                        </button>
                                </div>
                                <div class="col s12 m4 l4 margin-t">
                                    <button class="waves-effect green waves-light btn d-block tooltipped" data-position="top" data-tooltip="Add Contact <i class='icon-plus green-text text-lighteen-1'></i>">
                                        <span class="hide-on-small-only">Add</span> <i class="icon-plus"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 margin-t">
                        <ul class="collapsible popout" data-collapsible="accordion">
                            <li>
                                <div class="collapsible-header hoverable"><i class="material-icons">view_list</i><?php echo e($user->user_name); ?></div>
                                <div class="collection collapsible-body">
                                    <div class="collection-item"><b>Fisrt name:</b> <?php echo e($user->first_name); ?></div>
                                    <div class="collection-item"><b>Last name:</b> <?php echo e($user->last_name); ?></div>
                                    <?php /*<div class="collection-item">Email: <?php echo e($user->email); ?></div>*/ ?>
                                    <div class="collection-item"><b>Age:</b> <?php echo e($user->birthday ? $user->age : "Not degined"); ?></div>
                                    <div class="collection-item"><b>Sex:</b> <?php echo e($user->sex=='f' ? 'female' : 'male'); ?></div>
                                    <div class="collection-item"><b>Contry:</b> <?php echo e($user->profile->country ? $user->profile->country : "Not defined"); ?></div>
                                    <div class="collection-item"><b>State:</b> <?php echo e($user->profile->region ? $user->profile->region : "Not defined"); ?></div>
                                    <div class="collection-item"><b>Citie:</b> <?php echo e($user->profile->citie ? $user->profile->citie : "Not defined"); ?></div>
                                    <div class="collection-item">
                                        <p class="no-padding"><b>Description:</b></p>
                                        <p class="padding-tb">
                                            <?php echo e($user->profile->description ? $user->profile->description : "Not defined"); ?>

                                        </p>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php else: ?>
               <div class="col s12 white border-thin">
                   <h5>User Not found</h5>
               </div>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>