<?php
// Global constants

define('APP_NAME','Hazelcites');
define('DS',DIRECTORY_SEPARATOR);
define('ROOT_PATH', realpath(__DIR__.DS.".."));
define('APP_PATH', realpath(ROOT_PATH . DS . 'app'));
define('PUBLIC_PATH',realpath(__DIR__));