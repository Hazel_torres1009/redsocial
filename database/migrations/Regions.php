<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Regions extends Migration
{
    public function up()
    {
        Schema::create('regions', function(Blueprint $table){
            $table->increments('id');
            $table->char('country');
            $table->char('code');
            $table->string('name');
            $table->double('latitude');
            $table->double('longitude');
            $table->integer('cities');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('regions');
    }
}
