<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{

    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('object_id');
            $table->string('object_type');
            $table->enum('report_type', [0,1,2,3,4,5]);
            $table->string('status');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::drop('reports');
    }
}
