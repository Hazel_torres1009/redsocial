<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Countries extends Migration
{

    public function up()
    {
        Schema::create('countries', function(Blueprint $table){
            $table->increments('id');
            $table->char('iso');
            $table->string('name');
            $table->string('nicename');
            $table->char('iso3')->nullable();
            $table->smallInteger('numcode')->nullable();
            $table->integer('phonecode');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('countries');
    }
}
