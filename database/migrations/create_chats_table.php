<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatsTable extends Migration
{

    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('chan_name');
            $table->integer('user_id');
            $table->integer('message_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('chats');
    }
}
