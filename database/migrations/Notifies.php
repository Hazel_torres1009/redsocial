<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifies extends Migration
{
    public function up()
    {
        Schema::create('notifies', function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_from');
            $table->integer('user_to');

            $table->enum('status', [0,1,2,3,4])->nullable();

            $table->boolean('viewed')->default(false);
            $table->enum('type', [1,2,3,4,5,6,7,8,9]);

            $table->integer('object');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('notifies');
    }
}
